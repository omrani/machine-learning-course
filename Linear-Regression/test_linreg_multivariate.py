'''
    TEST SCRIPT FOR MULTIVARIATE LINEAR REGRESSION
'''

'''
Numpy is a standard library in python that lets you do matrix and vector operations like Matlab in python.
Check out documentation here: http://wiki.scipy.org/Tentative_NumPy_Tutorial
If you are a Matlab user this page is super useful: http://wiki.scipy.org/NumPy_for_Matlab_Users
'''
import numpy as np
from numpy.linalg import *

# our linear regression class
from linreg import LinearRegression


if __name__ == "__main__":
    '''
        Main function to test multivariate linear regression
    '''

    # load the data
    filePath = "data/multivariateData.dat"
    file = open(filePath,'r')
    allData = np.loadtxt(file, delimiter=',')


    X = np.matrix(allData[:,:-1])
    y = np.matrix((allData[:,-1])).T

    n,d = X.shape

    # Standardize
    mean = X.mean(axis=0)
    std = X.std(axis=0)
    X = (X - mean) / std

    # Add a row of ones for the bias term
    X = np.c_[np.ones((n,1)), X]

    # initialize the model
    init_theta = np.matrix(np.random.randn((d+1))).T
    n_iter = 2000
    alpha = 0.001

    # Instantiate objects
    lr_model = LinearRegression(init_theta = init_theta, alpha = alpha, n_iter = n_iter)
    lr_model.fit(X,y)




    #Load Test Data
    filePath = "data/holdout.npz"
    hold_data = np.load('data/holdout.npz', mmap_mode='r')
    X_test = np.matrix(hold_data['arr_0'][:, :-1])
    y_test = np.matrix((hold_data['arr_0'][:, -1])).T



    # Standardize
    mean = X_test.mean(axis=0)
    std = X_test.std(axis=0)
    X_test = (X_test - mean) / std


    n_test,d_test= X_test.shape

    X_test = np.c_[np.ones((n_test,1)), X_test]
    y_predict  = lr_model.predict(X_test)


    # Error computation

    test_loss = y_predict - y_test
    test_error = test_loss.T.dot(test_loss)/(2*len(test_loss))

    print("And here is the test error",test_error)
